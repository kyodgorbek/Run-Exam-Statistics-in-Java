# Run-Exam-Statistics-in-Java




import psJava.KeyIn;

public class RunExamStatistics {
  
  public static void main(String[] args){
   
   // Compute statistics if array is created
   ExamStatistics ex = new ExamStatistics(KeyIn.readInt("How many scores?"));
   
   // Read the scores
   ex.readScores();
   
   // Show results of statistics calculations
   System.out.println("The scores are :\n" + ex.toString());
   System.out.println("\nMinumum score : " + ex.findMin());
   System.out.println("\nMaximum score: " + ex.findMax ());
   double mean = ex.computeMean();
   System.out.println("\nMean score: " + mean);
   System.out.println("\nStandard deviation: " + ex.computeStandDev(mean));
  }
 }  
